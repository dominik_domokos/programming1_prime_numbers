#include <stdio.h>
#include <stdlib.h>

#define MIN (0)
#define MAX (2514)



static void line_breaker()
{
    int h_max = 30;
    for(int h = 0;h<h_max; h++)
    {
        if((h == h_max -1) || (h == 0)){
            printf("\n");
        }
        else { printf("_"); }

    }



}


static void prim(int min, int max){
    FILE *fptr;
    fptr =fopen("prime_numbers.txt", "w");
    if(fptr == NULL){
        printf("Error in file opening");
        exit(1);
    }

    int sum_count;
    printf("Prime numbers between %d and %d:", min, max);
    line_breaker();

  for(int Number = min; Number <= max; Number++)
  {
    int count = 0;
    for (int i = 2; i <= Number/2; i++)
        if(Number%i == 0)
        {
            count++;

            break;
        }

        if(count == 0 && Number != 1 ){
            printf(" %d ", Number);
            fprintf(fptr," %d ", Number);
            sum_count++;
        }
  }


  line_breaker();
  printf("All in all %d number(s)", sum_count);
  line_breaker();
  fclose(fptr);


}

int main()
{
   prim(MIN, MAX);
   return 0;


}
